﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ALSMGHSurvey.API
{
    public class MGHRequestObject
    {
        public string partner_code { get; set; }
        public string partner_auth { get; set; }
        public string app_code { get; set; }
        public string mode { get; set; }
        public string user_email { get; set; }
        public string hash1 { get; set; }
        public string hash2 { get; set; }
        public string hash3 { get; set; }
    }
}
