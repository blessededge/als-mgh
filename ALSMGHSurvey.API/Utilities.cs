﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ALSMGHSurvey.API
{
    public class Utilities
    {
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }
        public static string calculateSHA512(string input)
        {
            var hash = string.Empty;
            using (SHA512 shaM = new SHA512Managed())
            {
                var data = Encoding.UTF8.GetBytes(input);
                var ret = shaM.ComputeHash(data);
                hash = Convert.ToBase64String(ret);
            }
            return hash;
        }
        public static bool ValidateAPIKey(string APIKey)
        {
            var key = Environment.GetEnvironmentVariable("authorization_key");
            return key.Equals(APIKey);
        }
    }
}
