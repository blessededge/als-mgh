﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ALSMGHSurvey.API
{
    public class ALSRequest
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string middle_name { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string country_of_birth { get; set; }
        public string city_of_birth { get; set; }
    }
}
