﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ALSMGHSurvey.API
{
    public class MGHService
    {
        public static async Task<MGHReturnObject> GetNeuroStampKey(MGHRequestObject request)
        {
            var urlEndPoint = Environment.GetEnvironmentVariable("ApiUrl");

            using (var client = new HttpClient())
            {
                var postData = new List<KeyValuePair<string, string>>();
                postData.Add(new KeyValuePair<string, string>("hash1", request.hash1));
                postData.Add(new KeyValuePair<string, string>("hash2", request.hash2));
                postData.Add(new KeyValuePair<string, string>("hash3", request.hash3));
                postData.Add(new KeyValuePair<string, string>("app_code", Environment.GetEnvironmentVariable("app_code")));
                postData.Add(new KeyValuePair<string, string>("mode", Environment.GetEnvironmentVariable("mode")));
                postData.Add(new KeyValuePair<string, string>("partner_auth", Environment.GetEnvironmentVariable("partner_auth")));
                postData.Add(new KeyValuePair<string, string>("partner_code", Environment.GetEnvironmentVariable("partner_code")));
                postData.Add(new KeyValuePair<string, string>("user_email", Environment.GetEnvironmentVariable("user_email")));

                using (var content = new FormUrlEncodedContent(postData))
                {
                    content.Headers.Clear();
                    content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    HttpResponseMessage response = await client.PostAsync(urlEndPoint, content);
                    var returnObject = response.Content.ReadAsAsync<MGHReturnObject>().Result;
                    return returnObject;
                }
            }
        }
    }
}
