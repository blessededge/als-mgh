using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Buffers.Text;
using System.Collections.Generic;

namespace ALSMGHSurvey.API
{
    public static class GenerateMGHTokenFunction
    {
        [FunctionName("GenerateMGHToken")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            try
            {
                log.LogInformation("Starting MGH Generation");

                //Validate API Key
                IEnumerable<string> headerValues = req.Headers["Authorization"];
                var authorization = headerValues.FirstOrDefault();
                if (!Utilities.ValidateAPIKey(authorization))
                {
                    log.LogWarning("Invalid credentials passed to API");
                    throw new Exception("Invalid credentials passed to API");
                }

                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                var obj = JsonConvert.DeserializeObject<ALSRequest>(requestBody);
                var national_id_issuer = string.Empty;
                var national_id = string.Empty;

                if (string.IsNullOrEmpty(obj.middle_name))
                    obj.middle_name = "NOTAPPLICABLE";
                var row1 = "_" + Convert.ToDateTime(obj.dob).Year.ToString() + "_" + Convert.ToDateTime(obj.dob).Day.ToString() + "_" + obj.gender + "_" + national_id_issuer + national_id + "_";
                var row2 = "_" + obj.first_name + "_" + obj.last_name + "_" + obj.middle_name + "_" + obj.country_of_birth + obj.city_of_birth + "_" + Convert.ToDateTime(obj.dob).Day.ToString() + "_" + Convert.ToDateTime(obj.dob).Month.ToString() + "_";
                var row3 = "_" + obj.first_name + "_" + obj.last_name + "_" + Convert.ToDateTime(obj.dob).Year.ToString() + "_" + obj.country_of_birth + obj.city_of_birth + "_" + Convert.ToDateTime(obj.dob).Day.ToString() + "_" + Convert.ToDateTime(obj.dob).Month.ToString() + "_";

                var request = new MGHRequestObject();
                request.hash1 = Utilities.calculateSHA512(row1);
                request.hash2 = Utilities.calculateSHA512(row2); ;
                request.hash3 = Utilities.calculateSHA512(row3);
                log.LogInformation("End MGH Generation");

                var ret = await MGHService.GetNeuroStampKey(request);
                if (string.IsNullOrEmpty(ret.status) || ret.status.ToLower() != "ok")
                {
                    throw new Exception("Error executing API");
                }
                return new OkObjectResult(ret);
            }
            catch (Exception ex)
            {
                log.LogError(ex.Message);
                return new BadRequestObjectResult(new JsonResult(ex.Message));
            }
        }
    }
}
