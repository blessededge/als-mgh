﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ALSMGHSurvey.API
{
    public class MGHReturnObject
    {
        public string status { get; set; }
        public string result { get; set; }
        public List<object> messages { get; set; }
    }
}
